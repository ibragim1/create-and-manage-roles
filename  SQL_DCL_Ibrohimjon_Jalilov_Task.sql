-- 1 Create a new user with limited permissions:
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- 2 Grant SELECT permission for the "customer" table:
GRANT SELECT ON TABLE customer TO rentaluser;

-- 3 Create a new user group and add the user to the group:
CREATE GROUP rental;
GRANT rental TO rentaluser;

-- 4 Grant INSERT and UPDATE permissions for the "rental" table:
GRANT INSERT, UPDATE ON TABLE rental TO GROUP rental;

-- Perform the insert and update operations:
INSERT INTO rental(rental_date, inventory_id, customer_id, return_date)
VALUES ('2024-02-25', 1, 1, '2024-03-29');
UPDATE rental SET return_date = '2024-03-30' WHERE rental_id = 1;

-- 5 Revoke INSERT permission for the "rental" table:
REVOKE INSERT ON TABLE rental FROM GROUP rental;

-- 6 Create a personalized role for an existing customer:
-- Replace 'John' and 'Doe' with the specific customer's first and last name
CREATE ROLE client_Ibrohimjon_Jalilov;

-- Grant SELECT privilege on the rental table
GRANT SELECT ON TABLE rental TO client_Ibrohimjon_Jalilov;

-- Grant SELECT privilege on the payment table
GRANT SELECT ON TABLE payment TO client_Ibrohimjon_Jalilov;

-- Grant USAGE privilege on the public schema (replace with your actual schema name)
GRANT USAGE ON SCHEMA public TO client_Ibrohimjon_Jalilov;



SET ROLE client_Ibrohimjon_Jalilov;
-- Replace '35' with the actual customer ID you want to query
SELECT * FROM rental WHERE customer_id = 35;

-- Replace '25' with the actual customer ID you want to query
SELECT * FROM payment WHERE customer_id = 25;

